if defined JAVA_HOME goto foundJavaHome
echo ERROR: JAVA_HOME is not set. Please set the JAVA_HOME variable in your environment to match the echo location of your Java installation.
exit /b 1
:foundJavaHome

REM Put your TestFairy API_KEY here. Find it in your TestFairy account preferences.
REM API_KEY= %1

REM example: SET KEYSTORE_PATH="debug.keystore"
SET KEYSTORE_PATH="C:/Users/6100489/.android/debug.keystore"

REM example: SET STOREPASS="android"
SET STOREPASS= android

REM example: SET ALIAS="androiddebugkey"
SET ALIAS= androiddebugkey

REM example: APK_PATH="c:/temp/small.apk"
SET APK_PATH="%WORKSPACE%\app\build\outputs\apk\debug\app-debug.apk"

testfairy-uploader --api-key=%API_KEY% --keystore=%KEYSTORE_PATH% --storepass=%STOREPASS% --alias=%ALIAS% %APK_PATH%

