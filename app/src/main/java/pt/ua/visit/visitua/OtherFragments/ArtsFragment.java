package pt.ua.visit.visitua.OtherFragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import pt.ua.visit.visitua.HelperClasses.ParserJSON;
import pt.ua.visit.visitua.HelperClasses.TaskCanceler;
import pt.ua.visit.visitua.HelperClasses.WSaccess;
import pt.ua.visit.visitua.POJOS.Art;
import pt.ua.visit.visitua.R;
import static pt.ua.visit.visitua.HelperClasses.ConstantsClass.*;

/**
 * Fragmento que suporta a Página com as Obras de Arte.
 */
public class ArtsFragment extends Fragment {

    // View correspondente ao layout completo da página
    private View view;

    // Tarefa que executa em 2º plano e que obtém as Artes Públicas
    private getArts getArts;

    // Componentes que servem para cancelar a tarefa getArts,
    // se esta demorar mais de GET_TURNS_TASK_TIMEOUT milisegundos
    private Handler canceler;
    private TaskCanceler taskCanceler;

    /**
     * Retorna uma instância de ArtsFragment.
     * @return instância de ArtsFragment
     */
    public static ArtsFragment newInstance()
    {
        ArtsFragment fragment = new ArtsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState);}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_arts, container, false);

        // Inicializar os componentes necessários para a execução e possível
        // cancelamento da tarefa que obtém os dados do WS das Artes Públicas
        getArts = new getArts();
        canceler = new Handler();
        taskCanceler = new TaskCanceler(getArts, getActivity(), getString(R.string.taskCancelerText));
        canceler.postDelayed(taskCanceler, TASK_TIMEOUT);
        getArts.execute(this.getActivity());

        return view;
    }

    private class getArts extends AsyncTask<Context, Void, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute()
        {
            progressDialog= ProgressDialog.show(ArtsFragment.this.getActivity(), getString(R.string.progressDialogDataTitle),getString(R.string.progressDialogDataMessage), true);
        };

        @Override
        protected void onCancelled (String jsonResponse)
        {
            progressDialog.dismiss();
        }

        @Override
        protected String doInBackground(Context... params)
        {
            // Obter os dados do WS dos Edifícios
            WSaccess ws = new WSaccess();
            String jsonResponse =  "";
            jsonResponse = ws.get(getString(R.string.obrasarte));
            return jsonResponse;
        }

        protected void onPostExecute(String jsonResponse)
        {
            progressDialog.dismiss();
            canceler.removeCallbacks(taskCanceler);

            // Se a resposta for vazia
            if(jsonResponse.equals(""))
            {
                // Indicar que a tarefa falhou
                Toast.makeText(getActivity(), getString(R.string.taskCancelerText1), Toast.LENGTH_LONG).show();
                return;
            }
            // Senão, processar a resposta
            processTaskResults(jsonResponse);
        }
    }

    private void processTaskResults(String results)
    {
        // Efetuar o parsing da resposta do WS
        ParserJSON parser = new ParserJSON();
        final List<Art> art = parser.parseArt(results);

        if(art.size() == 0)
        {
            Toast.makeText(getActivity(), getString(R.string.taskCancelerText1), Toast.LENGTH_LONG).show();
            return;
        }

        // Apresentar a lista de Artes Públicas
        final ArrayAdapter<Art> artsAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_activated_1, art);
        final ListView artsListView = view.findViewById(R.id.artList);
        artsListView.setAdapter(artsAdapter);

        // Listener ativado quando um Arte Pública da lista é selecionado
        artsListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View item, int position, long rowId)
                    {
                        // Obter a Arte Pública selecionado com base na sua posição na lista
                        Art art = artsAdapter.getItem(position);

                        // Redirecionar para o Fragmento com a informação sobre a Arte Pública selecionada
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.home_layout, ArtFragment.newInstance(art));
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                }
        );
    }


}
