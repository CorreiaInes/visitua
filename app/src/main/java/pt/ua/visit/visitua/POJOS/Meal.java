package pt.ua.visit.visitua.POJOS;

import java.io.Serializable;
import java.util.HashMap;

/**
 * POJO Refeição.
 */
public class Meal implements Serializable {

    private String type;                    // Tipo
    private String date;                    // Data
    private String weekday;                 // Dia de semana
    private Integer weekdayNr;              // Número do dia de semana
    private String disabled;                // Indica se o refeitório está encerrado
    private HashMap<String,String> items;   // Lista de Pratos

    public Meal()
    {
        items = new HashMap<>();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getWeekday() {
        return weekday;
    }

    public void setWeekday(String weekday) {
        this.weekday = weekday;
    }

    public Integer getWeekdayNr() {
        return weekdayNr;
    }

    public void setWeekdayNr(Integer weekdayNr) {
        this.weekdayNr = weekdayNr;
    }

    public String getDisabled() {
        return disabled;
    }

    public void setDisabled(String disabled) {
        this.disabled = disabled;
    }

    public HashMap<String,String> getItems() {
        return items;
    }

    public void setItems(HashMap<String,String> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "Meal{" +
                "type='" + type + '\'' +
                ", date='" + date + '\'' +
                ", weekday='" + weekday + '\'' +
                ", weekdayNr=" + weekdayNr +
                ", disabled='" + disabled + '\'' +
                ", items=" + items +
                '}';
    }
}