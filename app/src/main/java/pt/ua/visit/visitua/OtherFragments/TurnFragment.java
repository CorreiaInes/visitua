package pt.ua.visit.visitua.OtherFragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import pt.ua.visit.visitua.POJOS.Turn;
import static pt.ua.visit.visitua.HelperClasses.ConstantsClass.*;
import pt.ua.visit.visitua.R;

/**
 * Fragmento que suporta a Página para cada tipo de Senha.
 */
public class TurnFragment extends Fragment {

    /**
     * Retorna uma instância de TurnFragment.
     * @param turn POJO Turn a enviar para o fragmento.
     * @return instância de TurnFragment.
     */
    public static TurnFragment newInstance(Turn turn)
    {
        TurnFragment fragment = new TurnFragment();
        Bundle args = new Bundle();
        args.putSerializable(TURN_KEY, turn);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_turn, container, false);

        // Obter o POJO Turn enviado
        Bundle args = getArguments();
        final Turn turn = (Turn) args.getSerializable(TURN_KEY);

        TextView nameView = view.findViewById(R.id.fragmentTitle);
        String sourceString = getString(R.string.turnType) + turn.getLetter();
        nameView.setText(sourceString);
        nameView.setTextColor(Color.BLACK);

        LinearLayout turnLayout = view.findViewById(R.id.turnLayout);

        // Demonstrar os dados no UI
        // Número da Senha
        if(turn.getNumber() != null)
        {
            TextView numberView = new TextView(getContext());
            sourceString = "<b>" + getString(R.string.turnNumber) + "</b>" + turn.getNumber();
            numberView.setText(Html.fromHtml(sourceString));
            numberView.setTextColor(Color.BLACK);
            numberView.setTextSize(15);
            turnLayout.addView(numberView);
        }

        // Designação da Senha
        if(turn.getDesignation() != null)
        {
            TextView designationView = new TextView(getContext());
            designationView.setText(turn.getDesignation());
            designationView.setTextColor(Color.BLACK);
            designationView.setTextSize(15);
            turnLayout.addView(designationView);
        }

        // Número de senhas em espera
        if(turn.getWaiting() != null)
        {
            TextView waitingView = new TextView(getContext());
            sourceString = "<b>" + getString(R.string.turnWait) + "</b>" + turn.getWaiting();
            waitingView.setText(Html.fromHtml(sourceString));
            waitingView.setTextColor(Color.BLACK);
            waitingView.setTextSize(15);
            turnLayout.addView(waitingView);
        }

        // Data de atendimento da última senha
        if(turn.getDate() != null)
        {
            TextView dateView = new TextView(getContext());
            sourceString = "<b>" + getString(R.string.turnLast) + "</b>" + turn.getDateString();
            dateView.setText(Html.fromHtml(sourceString));
            dateView.setTextColor(Color.BLACK);
            dateView.setTextSize(15);
            turnLayout.addView(dateView);
        }

        return view;
    }

}
