package pt.ua.visit.visitua.OtherFragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.net.URL;
import java.util.List;

import pt.ua.visit.visitua.HelperClasses.TaskCanceler;
import pt.ua.visit.visitua.HelperClasses.WSaccess;
import pt.ua.visit.visitua.HelperClasses.ParserJSON;
import pt.ua.visit.visitua.POJOS.OtherService;
import static pt.ua.visit.visitua.HelperClasses.ConstantsClass.*;
import pt.ua.visit.visitua.R;

/**
 * Fragmento que suporta a Página dos Outros Serviços.
 */
public class OtherServicesFragment extends Fragment implements OnMapReadyCallback {

    // Componentes do Mapa
    private MapView mMapView;
    private GoogleMap mGoogleMap;

    // Lista de Outros Serviços
    private List<OtherService> otherServices;

    // View correspondente ao layout completo da página
    private View view;

    // Tarefa que executa em 2º plano e que obtém os dados do WS dos Outros Serviços
    private getServices getServices;

    // Componentes que servem para cancelar a tarefa getServices,
    // se esta demorar mais de GET_SERVICES_TASK_TIMEOUT milisegundos
    private Handler canceler;
    private TaskCanceler taskCanceler;

    /**
     * Retorna uma instância de OtherServicesFragment.
     * @return instância de OtherServicesFragment.
     */
    public static OtherServicesFragment newInstance()
    {
        OtherServicesFragment fragment = new OtherServicesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_other_services, container, false);

        // Inicializar os componentes necessários para a execução e possível
        // cancelamento da tarefa que obtém os dados do WS dos Outros Serviços
        getServices = new getServices();
        canceler = new Handler();
        taskCanceler = new TaskCanceler(getServices, getActivity(), getString(R.string.taskCancelerText));
        canceler.postDelayed(taskCanceler, TASK_TIMEOUT);

        // Invocar a tarefa que obtém os dados do WS das Coleções
        getServices.execute(this.getActivity());

        return view;
    }

    /**
     * Tarefa que executa em 2º plano e que obtém os dados do WS dos Outros Serviços.
     */
    private class getServices extends AsyncTask<Context, Void, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute()
        {
            progressDialog= ProgressDialog.show(OtherServicesFragment.this.getActivity(), getString(R.string.progressDialogDataTitle),getString(R.string.progressDialogDataMessage), true);
        };

        @Override
        protected void onCancelled (String jsonResponse)
        {
            progressDialog.dismiss();
        }

        @Override
        protected String doInBackground(Context... params)
        {
            // Obter os dados do WS dos Outros Serviços
            WSaccess ws = new WSaccess();
            String jsonResponse =  "";
            jsonResponse = ws.get(getString(R.string.servicos));
            return jsonResponse;
        }

        protected void onPostExecute(String jsonResponse)
        {
            progressDialog.dismiss();
            canceler.removeCallbacks(taskCanceler);

            // Se a resposta for vazia
            if(jsonResponse.equals(""))
            {
                // Indicar que a tarefa falhou
                Toast.makeText(getActivity(), getString(R.string.taskCancelerText), Toast.LENGTH_LONG).show();
                return;
            }
            // Senão, processar a resposta
            processTaskResults(jsonResponse);
        }
    }

    private void processTaskResults(String results)
    {
        // Efetuar o parsing da resposta do WS
        ParserJSON parser = new ParserJSON();
        otherServices = parser.parseService(results);

        if(otherServices.size() == 0)
        {
            // Indicar que a tarefa falhou
            Toast.makeText(getActivity(), getString(R.string.taskCancelerText1), Toast.LENGTH_LONG).show();
            return;
        }

        // Demonstrar resultados na UI
        LinearLayout services_layout = view.findViewById(R.id.otherServices);
        for(OtherService s : otherServices)
        {
            Log.d("SERVICE", s.toString());

            final LinearLayout service_info = new LinearLayout(getContext());
            service_info.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams service_info_lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            service_info_lp.setMargins(10, 30, 30, 10);
            service_info.setLayoutParams(service_info_lp);

            // Designação do Serviço
            if(s.getDesignation() != null)
            {
                TextView designationView = new TextView(getContext());
                designationView.setTypeface(null, Typeface.BOLD);
                designationView.setText(s.getDesignation());
                designationView.setTextColor(Color.BLACK);
                service_info.addView(designationView);
            }

            // Obter a imagem do Serviço
            if(s.getId() != null) {
                try {
                    @SuppressLint("StaticFieldLeak") OtherServicesFragment.GetImage getImage = new OtherServicesFragment.GetImage(s.getId()){

                        @Override
                        protected void onPostExecute(Bitmap bmp) {
                            super.onPostExecute(bmp);

                            // Se a resposta for vazia
                            if(bmp == null)
                            {
                                return;
                            }
                            // Senão, processar a resposta
                            try
                            {
                                ImageView service_image = new ImageView(getContext());
                                service_image.setImageBitmap(bmp);

                                service_info.addView(service_image);
                            }
                            catch(Exception e)
                            {
                                return;
                            }
                        }
                    };

                    // Invocar a tarefa que obtém as imagens do WS dos Serviços
                    getImage.execute();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            // Local onde se encontra o Serviço
            if(s.getPlace() != null)
            {
                TextView placeView = new TextView(getContext());
                String sourceString = getString(R.string.otherservicesPlace) + s.getPlace();
                placeView.setText(sourceString);
                placeView.setTextColor(Color.BLACK);
                service_info.addView(placeView);
            }

            // Horário do Serviço
            if(s.getSchedule() != null)
            {
                TextView scheduleView = new TextView(getContext());
                String sourceString = getString(R.string.otherservicesSchedule) + s.getSchedule();
                scheduleView.setText(sourceString);
                scheduleView.setTextColor(Color.BLACK);
                service_info.addView(scheduleView);
            }

            services_layout.addView(service_info);
        }

        // Inicializar os componentes do Mapa
        mMapView = (MapView) view.findViewById(R.id.mapView);
        if (mMapView != null)
        {
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }
    }

    public class GetImage extends AsyncTask<Void, Void, Bitmap>{

        int serviceId;

        public GetImage(int id) {
            super();
            serviceId = id;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            // Obter imagem
            try {
                URL url = new URL(getString(R.string.servicosimagem) + serviceId);
                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                return bmp;
            } catch (Exception e) {
                // Uma das exceções enviadas (IllegalStateException) pode ser devida ao facto
                // de o fragmento ser fechado enquanto as imagens ainda estão a ser carregadas.
                // Porque após o seu carregamento, deixa de ser possível a sua demonstração no fragmento fechado.
                e.printStackTrace();
                return null;
            }

        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        MapsInitializer.initialize(getContext());

        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

        // Marcar o local de cada Serviço no Mapa
        for(OtherService service : otherServices)
        {
            String[] coordinatesArray = service.getCoordinates().split(",");
            Double latitude = Double.parseDouble(coordinatesArray[0].trim());
            Double longitude = Double.parseDouble(coordinatesArray[1].trim());

            LatLng servicos = new LatLng(latitude, longitude);
            mGoogleMap.addMarker(new MarkerOptions().position(servicos).title(service.getDesignation()));
            CameraPosition camera = CameraPosition.builder().target(servicos).zoom(15).bearing(0).tilt(45).build();
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(servicos,15));
        }

        // Listener invocado ao selecionar o Mapa
        mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener()
        {
            @Override
            public void onMapClick(LatLng arg0)
            {
                // Redirecionar o Utilizador para a página do Mapa com os Serviços marcados no mesmo
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.home_layout, pt.ua.visit.visitua.MenuFragments.MapFragment.newInstance(otherServices));
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        // Animações do Mapa
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(16), 2000, null);

    }

}

