package pt.ua.visit.visitua.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import pt.ua.visit.visitua.R;

import static pt.ua.visit.visitua.HelperClasses.ConstantsClass.SPLASH_TIME_OUT;

/**
 * Atividade que gere a página de introdução da aplicação.
 */
public class SplashActivity extends Activity {

    /**
     * View para o logótipo da aplicação
     */
    private ImageView logo;

    /**
     * View para o nome da aplicação
     */
    private TextView text;

    /**
     * Animações
     */
    private Animation uptodown, downtoup;


    /**
     * Método de criação da página.
     * @param savedInstanceState
     */
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // Definir o logótipo e o nome da aplicação
        logo = findViewById(R.id.logo);
        text = findViewById(R.id.text);

        // Definir as animações para o logótipo e nome
        downtoup = AnimationUtils.loadAnimation(this,R.anim.mytransition);
        uptodown = AnimationUtils.loadAnimation(this, R.anim.mytransition);
        text.setAnimation(uptodown);
        logo.setAnimation(downtoup);

        // Após o intervalo de tempo definido (SPLASH_TIME_OUT), carregar a página inicial da aplicação
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                startActivity(i);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
