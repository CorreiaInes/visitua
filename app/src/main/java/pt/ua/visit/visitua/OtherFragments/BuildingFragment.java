package pt.ua.visit.visitua.OtherFragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.Log;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.View;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URL;
import java.util.List;

import pt.ua.visit.visitua.HelperClasses.TaskCanceler;
import pt.ua.visit.visitua.MenuFragments.MapFragment;
import pt.ua.visit.visitua.POJOS.Building;
import pt.ua.visit.visitua.POJOS.Area;
import pt.ua.visit.visitua.R;
import static pt.ua.visit.visitua.HelperClasses.ConstantsClass.*;

/**
 * Fragmento que suporta a Página sobre cada Edifício.
 */
public class BuildingFragment extends Fragment {

    // POJO Edifício
    private Building building;

    // Layout utilizado para demonstrar as informações
    private LinearLayout buildingLayout;

    // View correspondente ao layout completo da página
    private View view;

    // Tarefa que executa em 2º plano e que obtém a imagem para a Obra de Arte
    private BuildingFragment.getImage getImage;

    // Componentes que servem para cancelar a tarefa getImage,
    // se esta demorar mais de GET_IMAGE_TASK_TIMEOUT milisegundos
    private Handler canceler;
    private TaskCanceler taskCanceler;

    /**
     * Retorna uma instância de BuildingFragment.
     * @param building POJO Building a enviar para o fragmento.
     * @return instância de BuildingFragment.
     */
    public static BuildingFragment newInstance(Building building)
    {
        BuildingFragment fragment = new BuildingFragment();
        Bundle args = new Bundle();
        args.putSerializable(BUILDING_KEY, building);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_building, container, false);

        // Obter o POJO Building enviado
        Bundle args = getArguments();
        building = (Building) args.getSerializable(BUILDING_KEY);
        Log.d("SELECTED BUILDING", building.toStringAll());

        // Inicializar os componentes necessários para a execução e possível
        // cancelamento da tarefa que obtém os dados do WS dos Edifícios
        getImage = new BuildingFragment.getImage();
        canceler = new Handler();
        taskCanceler = new TaskCanceler(getImage, getActivity(), getString(R.string.taskCancelerText4));
        canceler.postDelayed(taskCanceler, TASK_TIMEOUT);

        TextView nameView = view.findViewById(R.id.fragmentTitle);
        nameView.setText(building.getName());
        nameView.setTypeface(null, Typeface.BOLD);
        nameView.setTextSize(20);
        nameView.setGravity(Gravity.CENTER);
        nameView.setTextColor(Color.BLACK);

        buildingLayout = view.findViewById(R.id.buildingLayout);

        // Obter a imagem a partir do serviço
        getImage.execute();

        if(building.getCoordinates() != null)
        {
            // Botão para ver o Edifício em questão no Mapa
            Button mapa = new Button(getContext());
            mapa.setText(getString(R.string.mapButton));
            mapa.setBackgroundResource(R.drawable.button_style);
            buildingLayout.addView(mapa);

            // Listener ativado quando se clica no botão para ver no Mapa
            mapa.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    // Redireciona para a Página do Mapa, marcando o Edifício em questão no mesmo
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.home_layout, MapFragment.newInstance(building));
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            });
        }

        // Demonstrar resultados na UI
        // Iniciais do Nome do Edifício
        if(building.getInitials() != null)
        {
            TextView siglaView = new TextView(getContext());
            siglaView.setText(building.getInitials());
            siglaView.setTypeface(null, Typeface.BOLD);
            siglaView.setTextSize(30);
            siglaView.setGravity(Gravity.CENTER);
            siglaView.setTextColor(Color.BLACK);
            buildingLayout.addView(siglaView);
        }

        // Número, ou Letra, de identificação
        if(building.getId() != null)
        {
            TextView idView = new TextView(getContext());
            String sourceString = "<b>" + getString(R.string.buildingId) + "</b> " + building.getId();
            idView.setText(Html.fromHtml(sourceString));

            idView.setTextColor(Color.BLACK);
            buildingLayout.addView(idView);
        }

        // Tipo de Edifício
        if(building.getType() != null)
        {
            TextView typeView = new TextView(getContext());

            String sourceString = "<b>" + getString(R.string.buildingType) + "</b> " + building.getType();;
            typeView.setText(Html.fromHtml(sourceString));

            typeView.setTextColor(Color.BLACK);
            buildingLayout.addView(typeView);
        }

        //Data de construção
        if(building.getDate() != null)
        {
            TextView dateView = new TextView(getContext());

            String sourceString = "<b>" + getString(R.string.buildingConstructionDate) + "</b> " + building.getDate();
            dateView.setText(Html.fromHtml(sourceString));

            dateView.setTextColor(Color.BLACK);
            buildingLayout.addView(dateView);
        }

        // Descrição
        if(building.getDescription() != null)
        {
            TextView descriptionView = new TextView(getContext());
            descriptionView.setText(building.getDescription());

            descriptionView.setTextColor(Color.BLACK);
            buildingLayout.addView(descriptionView);
        }

        // Nome do Arquiteto
        if(building.getArchitect() != null)
        {
            TextView architectView = new TextView(getContext());

            String sourceString = "<b>" + getString(R.string.buildingArchitect) + "</b> " + building.getArchitect();
            architectView.setText(Html.fromHtml(sourceString));

            architectView.setTextColor(Color.BLACK);
            buildingLayout.addView(architectView);
        }

        // Áreas de Ensino
        List<Area> areas = building.getAreas();
        if(areas.size()>0)
        {
            TextView areasTitleView = new TextView(getContext());

            String sourceString = "<b>" + getString(R.string.buildingAreas) + "</b> ";
            areasTitleView.setText(Html.fromHtml(sourceString));

            areasTitleView.setTextColor(Color.BLACK);
            buildingLayout.addView(areasTitleView);

            for(Area a : areas)
            {
                TextView areaView = new TextView(getContext());
                sourceString = "<b>-> </b>" + a.getType() + ": " + a.getName();
                areaView.setText(Html.fromHtml(sourceString));
                areaView.setTextColor(Color.BLACK);
                buildingLayout.addView(areaView);
            }
        }

        return view;
    }

    /**
     * Tarefa que executa em 2º plano e que obtém a imagem para o Edifício.
     */
    private class getImage extends AsyncTask<Context, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(Context... params)
        {
            // Obter imagem
            try
            {
                URL url = new URL(getString(R.string.edificiosimagem) + building.getId());
                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                return bmp;
            }
            catch(Exception e)
            {
                return null;
            }
        }

        protected void onPostExecute(Bitmap bmp)
        {
            canceler.removeCallbacks(taskCanceler);

            // Se a resposta for vazia
            if(bmp == null)
            {
                // Indicar que a tarefa falhou
                Toast.makeText(getActivity(), "Não foi possível obter a imagem.", Toast.LENGTH_LONG).show();
                return;
            }
            // Senão, processar a resposta
            try
            {
                ImageView building_img = view.findViewById(R.id.image);
                building_img.setImageBitmap(bmp);
            }
            catch(Exception e)
            {
                Toast.makeText(getActivity(), "Não foi possível obter a imagem.", Toast.LENGTH_LONG).show();
            }
        }
    }

    // Cancelar a tarefa que obtém a imagem da Coleção ao fechar o fragmento.
    @Override
    public void onPause()
    {
        super.onPause();
        canceler.removeCallbacks(taskCanceler);
    }

}