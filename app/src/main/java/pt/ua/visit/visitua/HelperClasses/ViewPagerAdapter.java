package pt.ua.visit.visitua.HelperClasses;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import pt.ua.visit.visitua.POJOS.Meal;
import pt.ua.visit.visitua.R;

/**
 * Adaptador utilizado no ViewPager
 */
public class ViewPagerAdapter extends PagerAdapter{

    // Contexto da Aplicação
    private Context context;

    // Lista de POJO Refeição
    private List<Meal> meals;

    // Classe utilizada para converter a data no formato português
    private DateConverter dateConverter;

    // LayoutInflater utilizado para demonstrar os dados no ViewPager
    private LayoutInflater inflater;

    // Lingua utilizada no formato da data
    private final Locale ptLocale = new Locale("pt", "PT");

    /**
     * Construtor
     * @param context contexto
     * @param meals Lista de Refeições
     */
    public ViewPagerAdapter(Context context, List<Meal> meals){
        this.context = context;
        this.meals = meals;
    }

    // Retorna o número de Refeições
    @Override
    public int getCount() {
        return meals.size();
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position){

        // Views pa demonstrar os dados
        TextView dateView;
        TextView typeView;
        TextView disabledView;
        TextView itemsView;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.viewpager_item, container,false);

        dateConverter = new DateConverter();

        dateView = (TextView) itemView.findViewById(R.id.dateView);
        typeView = (TextView) itemView.findViewById(R.id.typeView);
        disabledView = (TextView) itemView.findViewById(R.id.disabledView);
        itemsView = (TextView) itemView.findViewById(R.id.itemsView);

        // Converter a data para o formato Português
        try {
            dateView.setText(dateConverter.convertDatePT(meals.get(position).getDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        typeView.setText(meals.get(position).getType());

        // Demonstrar os dados no ViewPager
        if(!meals.get(position).getDisabled().equalsIgnoreCase("0"))
        {
            disabledView.setText(meals.get(position).getDisabled());
        }
        else
        {
            HashMap<String,String> items = meals.get(position).getItems();
            Set set = items.entrySet();
            Iterator i = set.iterator();

            while(i.hasNext()) {
                Map.Entry me = (Map.Entry)i.next();
                String string = "<b>" + me.getKey() +"</b>";
                itemsView.append(Html.fromHtml(string));
                string = "\n" + me.getValue() + "\n";
                itemsView.append(string);
            }
        }

        ((ViewPager) container).addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        ((ViewPager) container).removeView((RelativeLayout) object);

    }
}
