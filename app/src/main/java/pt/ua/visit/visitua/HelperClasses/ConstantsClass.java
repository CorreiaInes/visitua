package pt.ua.visit.visitua.HelperClasses;

public class ConstantsClass {

    /**
     * Intervalo de tempo durante o qual é demonstrada a página de introdução da aplicação
     */
    public static final int SPLASH_TIME_OUT = 3 * 1000;

    /**
     * Nome das preferências relativas às notificações
     */
    public static final String NOTIFICATIONS_PREFERENCES_NAME = "Notifications";

    /**
     * Palavra chave para o Switch (On/Off) das notificações
     */
    public static final String NOTIFICATIONS_SWITCH_KEY = "switchKey";

    /**
     * Palavra chave para a indicação de que uma notificação foi enviada no Intent que corre a HomeActivity
     */
    public static final String NOTIFICATION_SENT_KEY = "notificationSent";

    /**
     * Palavra chave para a obra de arte que disparou uma notificação
     */
    public static final String NOTIFICATION_ART_KEY = "notificationArt";


    // Intervalo de tempo mínimo, em milisegundos, entre cada update da localização do Utilizador
    public static final int LOCATION_UPDATE_TIME_INTERVAL = 15 * 1000;

    // Intervalo de distância mínima, em metros, entre cada update da localização do Utilizador
    public static final int LOCATION_UPDATE_DISTANCE_INTERVAL = 0;

    // Distância máxima entre o Utilizador e uma obra de arte para este ser notificado
    public static final double MAX_ART_DISTANCE = 10.0;

    // Código utilizado nas permissões de localização
    public static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    // Referências para os POJOS
    public static final String BUILDING_KEY = "building";
    public static final String ART_KEY = "art";
    public static final String CANTEEN_KEY = "canteen";
    public static final String COLLECTION_KEY = "collection";
    public static final String TURN_KEY = "turn";

    // Intervalo de tempo máximo para a execução da tarefa (milisegundos)
    public static int TASK_TIMEOUT = 10 * 1000;

    // Intervalo de tempo entre as atualizações do fragmento
    public static int REFRESH_INTERVAL = 100 * 1000;

    // Variaveis referentes ao pedido de permissão do uso da Camara
    public static final int CAMERA_REQUEST = 1888;
    public static final int MY_CAMERA_PERMISSION_CODE = 100;

}
