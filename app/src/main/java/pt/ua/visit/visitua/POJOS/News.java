package pt.ua.visit.visitua.POJOS;

import java.io.Serializable;

/**
 * POJO Notícia.
 */
public class News implements Serializable{

    private String description;     //Descrição da notícia
    private String title;           // Título
    private String link;            // Link para a página da notícia
    private String date;            // Data de publicação

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public String getLink() { return link; }

    public void setLink(String link) { this.link = link; }

    public String getDate() { return date; }

    public void setDate(String date) { this.date = date; }

    @Override
    public String toString() {
        return "News{" +
                "description='" + description + '\'' +
                ", title='" + title + '\'' +
                ", link='" + link + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
