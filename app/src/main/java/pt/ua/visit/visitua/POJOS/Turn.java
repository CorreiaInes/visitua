package pt.ua.visit.visitua.POJOS;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * POJO Senha.
 */
public class Turn implements Serializable {

    private String letter;          // Letra
    private String designation;     // Designação
    private Integer number;         // Número da senha
    private Integer waiting;        // Número de senhas em espera
    private Date date;              // Data em formato yyyy-MM-dd HH:mm:ss

    public Turn() {
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getWaiting() {
        return waiting;
    }

    public void setWaiting(Integer waiting) {
        this.waiting = waiting;
    }

    public Date getDate() {
        return date;
    }

    public String getDateString()
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(date);
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Turn{" +
                "letter=" + letter +
                ", designation='" + designation + '\'' +
                ", number=" + number +
                ", waiting=" + waiting +
                ", date=" + getDateString() +
                '}';
    }

}
