package pt.ua.visit.visitua.OtherFragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.net.URL;

import pt.ua.visit.visitua.HelperClasses.TaskCanceler;
import pt.ua.visit.visitua.MenuFragments.MapFragment;
import pt.ua.visit.visitua.POJOS.Collection;
import pt.ua.visit.visitua.R;
import static pt.ua.visit.visitua.HelperClasses.ConstantsClass.*;

/**
 * Fragmento que suporta a Página sobre cada Coleção.
 */
public class CollectionFragment extends Fragment {

    // Layout utilizado para demonstrar as informações
    private LinearLayout collectionLayout;

    // POJO Coleção
    private Collection collection;

    // View correspondente ao layout completo da página
    private View view;

    // Tarefa que executa em 2º plano e que obtém a imagem para a Coleção
    private CollectionFragment.getImage getImage;

    // Componentes que servem para cancelar a tarefa getImage,
    // se esta demorar mais de GET_IMAGE_TASK_TIMEOUT milisegundos
    private Handler canceler;
    private TaskCanceler taskCanceler;

    /**
     * Retorna uma instância de CollectionFragment.
     * @param collection POJO Collection a enviar para o fragmento.
     * @return instância de CollectionFragment.
     */
    public static CollectionFragment newInstance(Collection collection)
    {
        CollectionFragment fragment = new CollectionFragment();
        Bundle args = new Bundle();
        args.putSerializable(COLLECTION_KEY,collection);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_collection, container, false);

        // Obter o POJO Collection
        Bundle args = getArguments();
        collection = (Collection) args.getSerializable(COLLECTION_KEY);
        Log.i("SELECTED COLLECTION", collection.toString());

        // Inicializar os componentes necessários para a execução e possível
        // cancelamento da tarefa que obtém as do WS das Coleções
        getImage = new CollectionFragment.getImage(collection.getId());
        canceler = new Handler();
        taskCanceler = new TaskCanceler(getImage, getActivity(), null);
        canceler.postDelayed(taskCanceler, TASK_TIMEOUT);


        TextView nameView = view.findViewById(R.id.collectionName);
        String sourceString = "<b>" + collection.getName() + "</b>";
        nameView.setText(Html.fromHtml(sourceString));
        nameView.setText(collection.getName());

        collectionLayout = view.findViewById(R.id.collectionLayout);
        setText(collectionLayout,collection);

        // Invocar a tarefa que obtém as imagens do WS das Coleções
        getImage.execute();


        if(collection.getPlaceCoordinates() != null)
        {
            // Botão para ver o Edifício em questão no Mapa
            Button mapa = new Button(getContext());
            mapa.setText(getString(R.string.mapButton));
            mapa.setBackgroundResource(R.drawable.button_style);
            collectionLayout.addView(mapa);

            // Listener ativado quando se clica no botão para ver no Mapa
            mapa.setOnClickListener(new MapButtonListener(collection,getContext(),getFragmentManager()));
        }

        // Apresentar as SubColeções, caso existam
        if(collection.getSubCollections() != null)
        {
            for (Collection c: collection.getSubCollections())
            {
                // Inicializar os componentes necessários para a execução e possível
                // cancelamento da tarefa que obtém as do WS das Coleções
                getImage = new CollectionFragment.getImage(c.getId());
                canceler = new Handler();
                taskCanceler = new TaskCanceler(getImage, getActivity(), null);
                canceler.postDelayed(taskCanceler, TASK_TIMEOUT);

                // Demonstrar resultados na UI
                TextView subCollectionNameView = new TextView(getContext());
                subCollectionNameView.setTextSize(18);
                sourceString = "<b>" + c.getName() + "</b>";
                subCollectionNameView.setText(Html.fromHtml(sourceString));
                subCollectionNameView.setTextColor(Color.BLACK);
                collectionLayout.addView(subCollectionNameView);
                setText(collectionLayout,c);

                // Invocar a tarefa que obtém as imagens do WS das Coleções
                getImage.execute();

                if(c.getPlaceCoordinates() != null)
                {
                    // Botão para ver o Edifício em questão no Mapa
                    Button mapa = new Button(getContext());
                    mapa.setText(getString(R.string.mapButton));
                    mapa.setBackgroundResource(R.drawable.button_style);
                    collectionLayout.addView(mapa);

                    // Listener ativado quando se clica no botão para ver no Mapa
                    mapa.setOnClickListener(new MapButtonListener(c,getContext(),getFragmentManager()));
                }

                TextView newline = new TextView(getContext());
                newline.setText(getString(R.string.newLine));
                collectionLayout.addView(newline);
            }
        }
        return view;
    }

    public void setText(LinearLayout layout, Collection collection)
    {
        // Demonstrar resultados na UI

        TextView descriptionView = new TextView(getContext());
        // Descrição da Coleção
        if(collection.getDescription() != null)
        {
            String sourceString = collection.getDescription() + getString(R.string.newLineHTML);
            descriptionView.setText(Html.fromHtml(sourceString));
            descriptionView.setTextColor(Color.BLACK);
            layout.addView(descriptionView);
        }
        else
        {
            descriptionView.append("\n");
        }

        // Doador da Coleção, caso seja conhecido
        if(collection.getDonator() != null)
        {
            TextView donatorView = new TextView(getContext());
            String sourceString = "<b>" + getString(R.string.collectionDonator) + "</b>" + collection.getDonator() + getString(R.string.newLineHTML);
            donatorView.setText(Html.fromHtml(sourceString));
            donatorView.setTextColor(Color.BLACK);
            layout.addView(donatorView);
        }

        // Local de visita
        if(collection.getVisitationPlace() != null)
        {
            TextView placeView = new TextView(getContext());
            String sourceString = "<b>" + getString(R.string.collectionPlace) + "</b>" + collection.getVisitationPlace() + getString(R.string.newLineHTML);
            placeView.setText(Html.fromHtml(sourceString));
            placeView.setTextColor(Color.BLACK);
            layout.addView(placeView);
        }
    }

    /**
     * Tarefa que executa em 2º plano e que obtém a imagem para a Coleção.
     */
    private class getImage extends AsyncTask<Context, Void, Bitmap> {

        int collectionId;

        public getImage(int id) {
            super();
            collectionId = id;
        }

        @Override
        protected Bitmap doInBackground(Context... params)
        {
            // Obter imagem
            try
            {
                URL url = new URL(getString(R.string.colecoesimagem) + collectionId);
                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                return bmp;
            }
            catch(Exception e)
            {
                return null;
            }
        }

        protected void onPostExecute(Bitmap bmp)
        {
            canceler.removeCallbacks(taskCanceler);

            // Se a resposta for vazia
            if(bmp == null)
            {
                return;
            }
            // Senão, processar a resposta
            try
            {
                ImageView collection_img = new ImageView(getContext());
                collection_img.setImageBitmap(bmp);
                collectionLayout.addView(collection_img);
            }
            catch(Exception e)
            {
            }
        }
    }

    // Cancelar a tarefa que obtém a imagem ao fechar o fragmento.
    @Override
    public void onPause()
    {
        super.onPause();
        canceler.removeCallbacks(taskCanceler);
    }

}
/**
 * Listener invocado ao carregar no botão "Ver no mapa" de uma coleção
 */
class MapButtonListener implements View.OnClickListener {

    private Collection collection;
    Context context;
    FragmentManager fragmentManager;

    /**
     * Construtor do Listener
     * @param collection coleção selecionada
     * @param context contexto da aplicação
     */
    public MapButtonListener(Collection collection, Context context, FragmentManager fragmentManager) {
        this.collection = collection;
        this.context = context;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public void onClick(View view) {
        // Redireciona para a Página do Mapa, marcando o Edifício em questão no mesmo
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.home_layout, MapFragment.newInstance(collection));
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
