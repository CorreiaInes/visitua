package pt.ua.visit.visitua.HelperClasses;

import android.os.StrictMode;
import android.util.Log;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Classe que lida com os pedidos HTTP enviados a Web Services.
 */
public class WSaccess {

    /**
     * Efetua o pedido HTTP GET para um dado URL de um WS.
     * @param url url do WS.
     * @return resposta do WS.
     */
    public String get(String url)
    {
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet get = new HttpGet(url);
        String result = "";

        try
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            String responseBody = httpClient.execute(get, responseHandler);

            result = responseBody;

        }catch (Exception e) {
            Log.d(this.getClass().getCanonicalName(), "WS GET FAILED!");
            e.printStackTrace();
            return "";
        }

        Log.d("WS GET RESULT:", result); // Imprimir resultados na consola

        return result;
    }

    /**
     * Efetua um pedido HTTP POST para um dado URL de um WS.
     * @param url url do WS.
     * @param urlParameters parâmetros do pedido.
     * @return resposta do WS.
     */
    public String sendPost(String url, String urlParameters)
    {
        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            con.setRequestProperty("Accept_Language", "en-US,en;q=0.5");
            con.setRequestProperty("Content-Type", "application/json");

            con.setDoOutput(true);
            DataOutputStream out = new DataOutputStream(con.getOutputStream());
            out.writeBytes(urlParameters);
            out.flush();
            out.close();

            int responseCode = con.getResponseCode();
            Log.d("WS POST", "URL: " + url);
            Log.d("WS POST", "PARAMS: " + urlParameters);
            Log.d("WS POST", "RESPONSE CODE: " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            String result = response.toString();

            Log.d("WS POST RESULT:", result); // Imprimir resultados na consola

            return result;
        }
        catch(Exception e)
        {
            Log.d(this.getClass().getCanonicalName(), "WS POST FAILED!");
            e.printStackTrace();
            return "";
        }
    }

}
