package pt.ua.visit.visitua.MenuFragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import pt.ua.visit.visitua.OtherFragments.ArtsFragment;
import pt.ua.visit.visitua.OtherFragments.CollectionListFragment;
import pt.ua.visit.visitua.OtherFragments.MuseologicalReserveFragment;
import pt.ua.visit.visitua.R;

/**
 * Fragmento que suporta a Página sobre o Museu da UA.
 */
public class MuseumFragment extends Fragment {

    // View correspondente ao layout completo da página
    private View view;

    /**
     * Retorna uma instância de MuseumFragment.
     * @return instância de MuseumFragment.
     */
    public static MuseumFragment newInstance()
    {
        MuseumFragment fragment = new MuseumFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_museum, container, false);

        Button artButton = view.findViewById(R.id.artButton);

        // Listener invocado ao clicar no botão Arte Pública
        artButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Redirecionar o Utilizador para a Página de Arte Pública
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.home_layout, ArtsFragment.newInstance());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        Button collections = view.findViewById(R.id.collectionsButton);

        // Listener invocado ao clicar no botão Arte Pública
        collections.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Redirecionar o Utilizador para a Página de Arte Pública
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.home_layout, CollectionListFragment.newInstance());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        Button reserveButton = view.findViewById(R.id.reserveButton);

        // Listener invocado ao clicar no botão Reserva Museológica
        reserveButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                // Redirecionar o Utilizador para a Página da Reserva Museológica
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.home_layout, MuseologicalReserveFragment.newInstance());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });


        return view;
    }
}
