package pt.ua.visit.visitua.POJOS;

/**
 * POJO Parque de Estacionamento.
 */
public class Park {

    private String name;        // Nome
    private Double latitute;    // Latitude pertencente às coordenadas
    private Double longitude;   // Longitude pertencente às coordenadas
    private Integer capacity;   // Capacidade
    private Integer occupied;   // Número de lugares ocupados
    private Integer free;       // Número de lugares livres

    public Park() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLatitute() {
        return latitute;
    }

    public void setLatitute(Double latitute) {
        this.latitute = latitute;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Integer getOccupied() {
        return occupied;
    }

    public void setOccupied(Integer occupied) {
        this.occupied = occupied;
    }

    public Integer getFree() {
        return free;
    }

    public void setFree(Integer free) {
        this.free = free;
    }

    @Override
    public String toString() {
        return "Park{" +
                "name='" + name + '\'' +
                ", latitute=" + latitute +
                ", longitude=" + longitude +
                ", capacity=" + capacity +
                ", occupied=" + occupied +
                ", free=" + free +
                '}';
    }

}
