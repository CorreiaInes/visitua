package pt.ua.visit.visitua.MenuFragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import pt.ua.visit.visitua.OtherFragments.BuildingsFragment;
import pt.ua.visit.visitua.R;

/**
 * Fragmento que suporta a Página sobre a UA.
 */
public class UAFragment extends Fragment {

    /**
     * Retorna uma instância de UAFragment.
     * @return instância de UAFragment.
     */
    public static UAFragment newInstance()
    {
        UAFragment fragment = new UAFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_ua, container, false);
        return view;
    }

}

