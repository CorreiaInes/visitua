package pt.ua.visit.visitua.OtherFragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import pt.ua.visit.visitua.HelperClasses.ViewPagerAdapter;
import pt.ua.visit.visitua.POJOS.Canteen;
import static pt.ua.visit.visitua.HelperClasses.ConstantsClass.*;
import pt.ua.visit.visitua.R;

/**
 * Fragmento que suporta a Página com as Refeições de um dado Refeitório.
 */
public class MealFragment extends Fragment implements OnMapReadyCallback {

    // POJO Cantina
    private Canteen canteen;

    // ViewPager utilizado para demonstrar as Refeições
    private ViewPager viewPager;

    // Adaptador utilizado para demonstrar os dados no ViewPager
    private PagerAdapter adapter;


    // Componentes para o Mapa
    private MapView mMapView;
    private GoogleMap mGoogleMap;

    /**
     * Retorna uma instância de MealFragment.
     * @param canteen POJO Canteen a enviar para o fragmento.
     * @return instância de MealFragment.
     */
    public static MealFragment newInstance(Canteen canteen)
    {
        MealFragment fragment = new MealFragment();
        Bundle args = new Bundle();
        args.putSerializable(CANTEEN_KEY, canteen);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_meal, container, false);

        // Obter o POJO Canteen enviado
        Bundle args = getArguments();
        canteen = (Canteen) args.getSerializable(CANTEEN_KEY);

        // Nome do Refeitório
        TextView nameView = view.findViewById(R.id.canteenName);
        nameView.setText(canteen.getName());

        // Inicializar o ViewPager e atribuir-lhe o adaptador
        viewPager = (ViewPager) view.findViewById(R.id.pager);
        adapter = new ViewPagerAdapter(getActivity(),canteen.getMeals());
        viewPager.setAdapter(adapter);

        // Botões utilizados para fazer a navegação pelo ViewPager
        ImageButton leftNav = (ImageButton) view.findViewById(R.id.left_nav);
        ImageButton rightNav = (ImageButton) view.findViewById(R.id.right_nav);

        // Listener invocado ao selecionar o botão da esquerda
        leftNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Mostra o item anterior no ViewPager
                int tab = viewPager.getCurrentItem();
                if (tab > 0) {
                    tab--;
                    viewPager.setCurrentItem(tab);
                } else if (tab == 0) {
                    viewPager.setCurrentItem(tab);
                }
            }
        });

        // Listener invocado ao selecionar o botão da direita
        rightNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Mostra o item seguinte no ViewPager
                int tab = viewPager.getCurrentItem();
                tab++;
                viewPager.setCurrentItem(tab);
            }
        });

        // Inicializar componentes do Mapa
        mMapView = view.findViewById(R.id.mapView);
        if (mMapView != null)
        {
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync((OnMapReadyCallback) this);
        }

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        MapsInitializer.initialize(getContext());

        // Componentes do Mapa
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

        // Marcar o local do Refeitório no Mapa
        if(canteen != null && canteen.getCoordinates() != null)
        {
            String[] coordsArray = canteen.getCoordinates().split(",");
            double latitude = Double.parseDouble(coordsArray[0].trim());
            double longitude = Double.parseDouble(coordsArray[1].trim());
            LatLng park = new LatLng(latitude, longitude);
            mGoogleMap.addMarker(new MarkerOptions().position(park).title(canteen.getName()));
            CameraPosition camera = CameraPosition.builder().target(park).zoom(15).bearing(0).tilt(45).build();
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(park,15));
        }

        // Listener invocado ao selecionar o mapa
        mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener()
        {
            @Override
            public void onMapClick(LatLng arg0)
            {
                // Redirecionar o Utilizador para o Fragmento do Mapa, onde vai estar marcado o
                // Refeitório
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.home_layout, pt.ua.visit.visitua.MenuFragments.MapFragment.newInstance(canteen));
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        // Animações do Mapa
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(16), 2000, null);
    }
}